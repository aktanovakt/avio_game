import pygame
import sys
import requests
from game import main

pygame.init()
pygame.display.set_caption('ООО "ШКОЛА БЕСПИЛОТНОЙ АВИАЦИИ"')
icon = pygame.image.load('images/plane.png')
pygame.display.set_icon(icon)

screen_width = 800
screen_height = 600

input_box_width = 400
input_box_height = 50

font = pygame.font.Font(None, 32)

white = (33, 33, 33)
black = (216, 216, 216)
gray = (128, 128, 128)

screen = pygame.display.set_mode((screen_width, screen_height))

username_input_box_x = screen_width / 2 - input_box_width / 2
username_input_box_y = screen_height / 2 - input_box_height / 2 - 50

username = ''

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                print(f"Username: {username}")
                if requests.get(f'http://127.0.0.1:8000/exists/{username}/').status_code == 200:
                    main(username=username)
                    pygame.quit()
                else:
                    pygame.quit()
                username = ''   
            elif event.key == pygame.K_BACKSPACE:
                if username_input_box_active:
                    username = username[:-1]
            else:
                if username_input_box_active:
                    username += event.unicode

    mouse_x, mouse_y = pygame.mouse.get_pos()

    username_input_box_active = username_input_box_x <= mouse_x <= username_input_box_x + input_box_width and \
                             username_input_box_y <= mouse_y <= username_input_box_y + input_box_height

    screen.fill(white)
    pygame.draw.rect(screen, gray if username_input_box_active else black, (username_input_box_x, username_input_box_y, input_box_width, input_box_height))

    email_text = font.render(username, True, white)
   
    screen.blit(email_text, (username_input_box_x + 10, username_input_box_y + 10))

    pygame.display.update()