import json
import pygame
import sys
from grafics import create_grafic
from sender import send_telemetry

pygame.init()
pygame.display.set_caption('ООО "ШКОЛА БЕСПИЛОТНОЙ АВИАЦИИ"')

icon = pygame.image.load('images/plane.png')
pygame.display.set_icon(icon)

dron = pygame.image.load('images/dron.png')
dron = pygame.transform.scale(dron, (99, 99))

screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))

background = pygame.image.load('images/sky.jpg')
background = pygame.transform.scale(background, (screen_width, screen_height))






def main(username):

    object_width = 50
    object_height = 50
    object_x = screen_width / 2 - object_width / 2
    object_y = screen_height / 2 - object_height / 2
    object_speed = 1    

    telemetry_list = []
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                create_grafic()
                send_telemetry(username)
                pygame.quit()
                sys.exit()

        key = []
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            object_x -= object_speed
            key.append('left')
        if keys[pygame.K_RIGHT]:
            object_x += object_speed
            key.append('right')
        if keys[pygame.K_UP]:
            object_y -= object_speed
            key.append('up')
        if keys[pygame.K_DOWN]:
            object_y += object_speed
            key.append('down')
        
        object_x = max(0, min(object_x, screen_width - object_width))
        object_y = max(0, min(object_y, screen_height - object_height))
        

        screen.blit(background, (0, 0))
        screen.blit(dron, (object_x, object_y))
        pygame.display.update()


        telemetry_data = {
            'time': pygame.time.get_ticks() / 1000,
            'position_x': object_x,
            'position_y': object_y,
            'key': key,
        }
        telemetry_list.append(telemetry_data)

        with open('data/telemetry.json', 'w') as f:
            json.dump(telemetry_list, f)
