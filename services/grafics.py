import json
import matplotlib.pyplot as plt 

def create_grafic():
    with open('data/telemetry.json', 'r+') as f:
        telemetry_data = json.loads(f.read())


    positions_x = [data['position_x'] for data in telemetry_data]
    positions_y = [data['position_y'] for data in telemetry_data]
    times = [data['time'] / 1000 for data in telemetry_data]


    plt.plot(times, positions_x, label='Position X')
    plt.plot(times, positions_y, label='Position Y')


    plt.xlabel('Per Second')
    plt.ylabel('Position')
    plt.title('Object Position Over Time')
    plt.legend()

    plt.savefig('data/grafics/telemetry_graf.png')
