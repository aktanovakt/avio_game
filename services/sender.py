import requests

url = 'http://127.0.0.1:8000/telemetry/'


def send_telemetry(username):
    with open('data/telemetry.json', 'rb') as telemetry_file, \
            open('data/grafics/telemetry_graf.png', 'rb') as telemetry_graf_file:

        data = {
            'user': username,
        }

        files = {
            'telemetry': telemetry_file,
            'telemetry_graf': telemetry_graf_file,
        }

        response = requests.post(url, data=data, files=files)
    